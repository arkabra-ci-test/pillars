FROM debian:bookworm-slim as buildStage

RUN apt update \
    && apt install --assume-yes cmake g++ libbox2d-dev libavcodec-dev \
       libavdevice-dev libavfilter-dev libavformat-dev libswscale-dev libx264-dev \
       libcairo2-dev liblog4cxx-dev libmicrohttpd-dev libfmt-dev
WORKDIR /building
COPY .. .

RUN mkdir build && cd build && cmake ..
RUN cd build && make

FROM debian:bookworm-slim

RUN apt update \
    && apt install --assume-yes libbox2d2 libavcodec59 \
       libavdevice59 libavfilter8 libavformat59 libswscale6 libx264-164 \
       libcairo2 liblog4cxx15 libmicrohttpd12 libfmt9 valgrind \
    && apt clean && apt-get clean

EXPOSE 8080/tcp

WORKDIR /app
COPY --from=buildStage /building/build/src/pillar .
COPY --from=buildStage /building/build/src/*.png .

CMD "/app/pillar"
