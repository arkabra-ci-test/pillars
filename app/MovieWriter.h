#pragma once

#include <stdint.h>

extern "C" {
    // libavcodec n'est pas C++-ready...
	#include <x264.h>
	#include <libswscale/swscale.h>
	#include <libavcodec/avcodec.h>
	#include <libavutil/mathematics.h>
	#include <libavformat/avformat.h>
	#include <libavutil/opt.h>
}
#include <cairo.h>

/**
 * @brief Crèe des films à partir d'une série de surfaces cairo.
 * 
 */
class MovieWriter {
    public:
        /**
         * @brief Démarre un film
         * 
         * @param filename Nom du fichier
         * @param width Largeur du film
         * @param height Hauteur du film
         * @param framerate Images/seconde
         */
        MovieWriter(const unsigned int width,
                    const unsigned int height,
                    const int framerate=60) ;

        ~MovieWriter() ;

        void flush() ;

        /**
         * @brief Ajoute une image au film
         * 
         * @param surface Une surface cairo qui sera rendue dans l'image.
         */
        void pushSurface(cairo_surface_t* surface) ;

        uint8_t* getResult(size_t& p) {
            p = resultMax ;
            return result ;
        }

    private:
        static int writeBuffer(void* opaque, uint8_t* buffer, int length) ;
        static int64_t seekBuffer(void* opaque, int64_t offset, int whence) ;

        bool pushBuffer(AVFrame* frame) ;

        int writeBuffer(uint8_t* buffer, int length) ;
        int64_t seekBuffer(int64_t offset, int whence) ;

        const unsigned int width ; ///< Largeur des images
        const unsigned int height ; ///< Hauteur des images
        const int framerate ;

        cairo_surface_t* work ;
        uint8_t* ioBuffer ;
	    SwsContext* swsCtx;
        const AVOutputFormat* outputFormat ;
        AVStream* stream;
        AVFormatContext* formatContext;
        AVCodecContext* codecContext;

        AVFrame* rgbpic ;
        AVFrame* yuvpic;

        uint8_t* result ;
        size_t resultSize ;
        size_t resultMax ;
        size_t resultCurrent ;

        int iframe ;
} ;
