#include <iostream>
#include <box2d/box2d.h>
#include <math.h>
#include <cairo.h>
#include <cairo-svg.h>

#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <microhttpd.h>

#include <fmt/format.h>
#include <log4cxx/fmtlayout.h>
#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>

#include "MovieWriter.h"
#include "PillarModel.h"

static auto logger = log4cxx::Logger::getLogger("pillars") ;

class RequestHandler {
    public:
        RequestHandler(struct MHD_Connection* connection,
                       const char* url,
                       const char* method,
                       const char* version,
                       const char* uploadData,
                       size_t* uploadDataSize) {
            setDefaultPillars() ;
            width=800 ;
            height = 600 ;
            fps = 60 ;
            simTime = 5 ;
            const MHD_ConnectionInfo* info ;
            sockaddr* clientAddress ;

            info = MHD_get_connection_info(connection, MHD_CONNECTION_INFO_CLIENT_ADDRESS) ;
            clientAddress = info->client_addr ;
 
            LOG4CXX_INFO_FMT(logger, "{} {} {}", clientAddress->sa_family, method, url) ;

            // On va examiner toutes les valeurs présentes dans les arguments URL
            // TODO: implémenter un jour le POST
            MHD_get_connection_values(connection, 
                                      MHD_GET_ARGUMENT_KIND, 
                                      parameterIterator, this) ;

            if (!strcasecmp(url, "/svg")) {
                mode = Mode::SVG ;
            } else if (!strcasecmp(url, "/movie")) {
                mode = Mode::MOVIE ;
            } else {
                mode = Mode::PNG ;
            }
        }

        ~RequestHandler() {
        }

        enum class Mode {
            SVG, PNG, MOVIE
        } ;

        int getHeight() const {return height ;}
        int getWidth() const {return width ;}
        int getFPS() const {return fps ;}
        float getSimTime() const {return simTime ;}
        std::vector<float>& getPillars() {return pillars;}
        Mode getMode() const {return mode;}

    private:

        /**
         * @brief Itérateur pour les paramètres de l'URL
         * 
         * @param cls Pointeur sur un RequestHandler
         * @param kind Type de paramètre: MHD_GET_ARGUMENT_KIND ou MHD_POSTDATA_KIND
         * @param key La clef du paramètre
         * @param value La valeur ou NULL
         * @return MHD_Result Toujours MHD_YES
         */
        static MHD_Result parameterIterator(void* cls,
                    enum MHD_ValueKind kind,
                        const char *key,
                        const char *value) {
            RequestHandler* that = (RequestHandler*)cls ;

            if ((kind==MHD_GET_ARGUMENT_KIND) && value) {
                if (!strcasecmp(key, "width") && value) {
                    that->width = atoi(value) ;
                } else if (!strcasecmp(key, "height") && value) {
                    that->height = atoi(value) ;
                } else if (!strcasecmp(key, "fps") && value) {
                    that->fps = atoi(value) ;
                } else if (!strcasecmp(key, "pillars") && value) {
                    that->parsePillars(value) ;
                } else if (!strcasecmp(key, "simtime") && value) {
                    that->simTime = atof(value) ;
                }
            }

            return MHD_YES ;
        }

        void setDefaultPillars() {
            // Les valeurs par défaut de nos piliers
            pillars.clear() ;
            for (auto i = 0; i < 4 ; i++) {
                pillars.push_back(1.0f) ;
            }
        }

        void parsePillars(const char* value) {
            char* dup ;
            char* temp ;
            pillars.clear() ;

            if ((temp = dup = strdup(value))) {
                char* token ;

                while ((token = strsep(&temp, ","))) {
                    float f = atof(token) ;
                    if (f < 0.0f) {
                        f = 0 ;
                    } else if (f > 1.0f) {
                        f = 1.0f ;
                    }

                    pillars.push_back(f) ;
                }

                free(dup) ;
            }

            if (pillars.size() < 1) {
                setDefaultPillars() ;
            }
        }

        int width ;
        int height ;
        int fps ;
        float simTime ;
        Mode mode ;
        std::vector<float> pillars ;
} ;

static cairo_status_t cairoPipeWriter(void* closure, const unsigned char* data, unsigned int length) {
    int* ends = (int*)closure ;
    size_t out ;

    out = write(ends[1], data, length) ;
    LOG4CXX_DEBUG_FMT(logger, "cairoPipeWriter a écrit {} bytes ({} demandés)", out, length) ;

    return out==length ? CAIRO_STATUS_SUCCESS : CAIRO_STATUS_WRITE_ERROR ;
}

static uint8_t* buildMovie(int width, int height, RequestHandler &handler, PillarModel &model, size_t &len);

static MHD_Result connectionHandler(void *cls,
                                    struct MHD_Connection *connection,
                                    const char *url,
                                    const char *method, const char *version,
                                    const char *upload_data,
                                    size_t *upload_data_size, void **con_cls) {
    struct MHD_Response *response = NULL ;
    int statusCode = MHD_HTTP_INTERNAL_SERVER_ERROR ;
    char buffer[1024] ;
    uint8_t* result = NULL ;

    MHD_Result ret = MHD_NO ;
    RequestHandler handler(connection, url, method, version, upload_data, upload_data_size) ;

    int width = handler.getWidth() ;
    int height = handler.getHeight() ;
    int fps = handler.getFPS() ;
    std::vector<float>& pillars = handler.getPillars() ;
    int pillarCount = pillars.size() ;
    float* pillarsHeights = &pillars[0] ;

    PillarModel model(pillarsHeights, pillarCount) ;

    cairo_surface_t* output_surface ;

    if (handler.getMode() == RequestHandler::Mode::SVG
      || handler.getMode() == RequestHandler::Mode::PNG) {
        for (auto i = 0 ; i < (int)(handler.getSimTime()*60); i++) {
            model.step(1.0f/60.0f) ;
        }

        int pipeEnds[2] ;

        if (!pipe(pipeEnds)) {
            const char* mime ;

            if (handler.getMode() == RequestHandler::Mode::SVG) {

                LOG4CXX_INFO(logger, "J'envoie un SVG") ;

                output_surface = cairo_svg_surface_create_for_stream(cairoPipeWriter,
                pipeEnds, width, height) ;

                model.render(output_surface, width, height) ;
                mime = "image/svg+xml" ;
            } else {
                output_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height) ;
                model.render(output_surface, width, height) ;

                LOG4CXX_INFO(logger, "J'envoie un PNG") ;

                cairo_surface_write_to_png_stream(output_surface, cairoPipeWriter, pipeEnds) ;
                mime = "image/png" ;
            }

            cairo_surface_destroy(output_surface) ;
            close(pipeEnds[1]) ;

            if ((response = MHD_create_response_from_pipe(pipeEnds[0]))) {
                MHD_add_response_header(response, "Content-Type", mime) ;
                statusCode = MHD_HTTP_OK ;
            } else {
                LOG4CXX_ERROR(logger, "MHD_create_response_from_pipe a rendu null") ;
                ret = MHD_NO ;
            }
        } else {
            snprintf(buffer, sizeof(buffer), "pipe error %d", errno) ;
            response = MHD_create_response_from_buffer (strlen (buffer),
                                            (void*) buffer, MHD_RESPMEM_PERSISTENT);
            MHD_add_response_header(response, "Content-Type", "text/plain") ;
        }
    } else if (handler.getMode() == RequestHandler::Mode::MOVIE) {
        size_t len ;
        result = buildMovie(width, height, handler, model, len);

        LOG4CXX_DEBUG_FMT(logger, "movie: obtenu un buffer de {} bytes", len) ;

        response = MHD_create_response_from_buffer (len,
                                            (void*) result, MHD_RESPMEM_PERSISTENT);
        MHD_add_response_header(response, "Content-Type", "video/mp4") ;
    }

    if (response) {
            LOG4CXX_DEBUG(logger, " -- avant MHD_queue_response()") ;
            ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
            LOG4CXX_DEBUG(logger, " -- après MHD_queue_response()") ;

            MHD_destroy_response (response);

            if (result) {
                free(result) ;
            }
        } else {
            ret = MHD_NO ;
        }

    return ret;
}

static uint8_t* buildMovie(int width, int height, RequestHandler &handler, PillarModel &model, size_t &len) {
    MovieWriter out(width, height, 60);
    cairo_t *context;
    cairo_surface_t* output_surface ;

    output_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
    context = cairo_create(output_surface);
    cairo_set_source_rgb(context, 1, 1, 0.8);

    for (auto i = 0; i < (int)(handler.getSimTime() * 60); i++)
    {
        cairo_paint(context);
        model.render(output_surface, width, height);

        out.pushSurface(output_surface);
        model.step(1.0f / 60.0f);
    }
    cairo_destroy(context);

    out.flush();
    return out.getResult(len);
}

int main(int argc, const char** argv) {
    struct MHD_Daemon *daemon;

    log4cxx::BasicConfigurator::configure() ;
    logger->setLevel(log4cxx::Level::getInfo());

    LOG4CXX_INFO(logger, "J'ecoute sur le port 8080") ;
  
    if (!(daemon = MHD_start_daemon(MHD_USE_INTERNAL_POLLING_THREAD, 
                                    8080,
                                    NULL, NULL,
                                    &connectionHandler,
                                    NULL,
                                    MHD_OPTION_END))) {
        return -1 ;
    }

  while (1) ;
}
