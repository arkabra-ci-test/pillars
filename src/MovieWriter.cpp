#include "MovieWriter.h"
#include <iostream>

static int64_t current=0 ;

int MovieWriter::writeBuffer(void* opaque, uint8_t* buffer, int length) {
    return ((MovieWriter*)opaque)->writeBuffer(buffer, length) ;
}

int MovieWriter::writeBuffer(uint8_t* buffer, int length) {

    if ((length + resultCurrent) > resultSize) {
        resultSize = length + resultCurrent + 65536 ;
        result = (uint8_t*)realloc(result, resultSize) ;
    }

    memcpy(result+resultCurrent, buffer, length) ;
    resultCurrent += length ;
    if (resultCurrent > resultMax) {
        resultMax = resultCurrent ;
    }

    current += length ;
    return length ;
}

int64_t MovieWriter::seekBuffer(void* opaque, int64_t offset, int whence) {
    return ((MovieWriter*)opaque)->seekBuffer(offset, whence) ;
}

int64_t MovieWriter::seekBuffer(int64_t offset, int whence) {
    if (whence) {
        fprintf(stderr, "whence pas supporté: %d\n", whence) ;
    } else {
        resultCurrent = offset ;

        if (resultCurrent > resultSize) {
            resultMax = resultSize = resultCurrent ;
            result = (uint8_t*)realloc(result, resultSize) ;
        }
    }

    return resultCurrent ;
}

MovieWriter::MovieWriter(const unsigned int width,
                    const unsigned int height,
                    const int framerate) 
    : height{height}, width{width}, framerate{framerate}, iframe{0} {
        work = cairo_image_surface_create(CAIRO_FORMAT_RGB24,
                                          width,
                                          height) ;

        av_log_set_level(AV_LOG_QUIET) ;
        
        swsCtx = sws_getContext(width, height,
		                        AV_PIX_FMT_RGB24, width, height,
                                AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR,
                                NULL, NULL, NULL) ;

        
    	outputFormat = av_guess_format("mp4", NULL, NULL) ;

        avformat_alloc_output_context2(&formatContext, outputFormat, NULL, NULL) ;

        const AVCodec* codec = avcodec_find_encoder(AV_CODEC_ID_H264) ;

        AVDictionary* opt = NULL;

        codecContext = avcodec_alloc_context3(codec);
        if (!codecContext) {
            fprintf(stderr, "Could not allocate video codec context\n");
            exit(-1);
        }

        codecContext->width = width;
        codecContext->height = height;
        codecContext->pix_fmt = AV_PIX_FMT_YUV420P;
        codecContext->time_base = (AVRational){ 1, framerate };

        stream = avformat_new_stream(formatContext, codec);
        stream->time_base = (AVRational){ 1, framerate };

        if (avcodec_open2(codecContext, codec, &opt) < 0) {
            fprintf(stderr, "Could not open codec\n");
            exit(-1);
        }

        if (avcodec_parameters_from_context(stream->codecpar, codecContext) < 0) {
            fprintf(stderr, "Could not initialize stream parameters\n");
            exit(-1);
        }

        size_t bufferSize = 8192 ;
        if (!(ioBuffer = (uint8_t*)av_malloc(bufferSize))) {
            fprintf(stderr, "Failed to allocate ioBuffer\n");
            exit(-1);
        }        

        resultSize = 32768;
        resultMax = resultCurrent=0 ;
        result = (uint8_t*)malloc(resultSize) ;

        formatContext->pb = avio_alloc_context(ioBuffer, bufferSize,
                                               true, this, NULL,
                                               writeBuffer, seekBuffer) ;

        formatContext->flags |= AVFMT_FLAG_CUSTOM_IO | AVFMTCTX_UNSEEKABLE;

        int ret = avformat_write_header(formatContext, &opt);
        av_dict_free(&opt);

        // Preparing the containers of the frame data:
        // Allocating memory for each RGB frame, which will be lately converted to YUV.
        rgbpic = av_frame_alloc();
        rgbpic->format = AV_PIX_FMT_RGB24;
        rgbpic->width = width;
        rgbpic->height = height;
        ret = av_frame_get_buffer(rgbpic, 1);

        // Allocating memory for each conversion output YUV frame.
        yuvpic = av_frame_alloc();
        yuvpic->format = AV_PIX_FMT_YUV420P;
        yuvpic->width = width;
        yuvpic->height = height;
        ret = av_frame_get_buffer(yuvpic, 1);
}   

MovieWriter::~MovieWriter() {
	// Freeing all the allocated memory:
	sws_freeContext(swsCtx);
	av_frame_free(&rgbpic);
	av_frame_free(&yuvpic);
	avcodec_free_context(&codecContext);
	avformat_free_context(formatContext);
    av_free(ioBuffer) ;
    free(result) ;

	cairo_surface_destroy(work);
}

void MovieWriter::flush() {
	while (1) {
        AVPacket* pkt = av_packet_alloc() ;

		int ret = avcodec_send_frame(codecContext, NULL);
		if (ret == AVERROR_EOF)
			break;
		else if (ret < 0) {
			fprintf(stderr, "Error sending frame to codec, errcode = %d\n", ret);
			exit(-1);
		}

		ret = avcodec_receive_packet(codecContext, pkt);
		if (ret < 0 && ret != AVERROR(EAGAIN) && ret != AVERROR_EOF) {
			fprintf(stderr, "Error receiving packet from codec, errcode = %d\n", ret);
			exit(-1);
		} else if (ret >= 0) {
			fflush(stdout);
			av_packet_rescale_ts(pkt, (AVRational){ 1, framerate }, stream->time_base);
			pkt->stream_index = stream->index;
			av_interleaved_write_frame(formatContext, pkt);
			av_packet_free(&pkt);
		} else
			break;
	}

	// Writing the end of the file.
	av_write_trailer(formatContext);
}

void MovieWriter::pushSurface(cairo_surface_t* surface) {
    cairo_t* context ;

    // On copie la surface dans notre image de travail
    context = cairo_create(work) ;
    cairo_set_source_rgb(context, 1, 1, 1) ;
    cairo_paint(context) ;
    cairo_set_source_surface(context, surface, 0, 0) ;
    cairo_paint(context) ;
    cairo_destroy(context) ;

    unsigned char* data = cairo_image_surface_get_data(work) ;

    for (unsigned int y = 0; y < height; y++) {
        for (unsigned int x = 0; x < width; x++) {
            // rgbpic->linesize[0] is equal to width.
            rgbpic->data[0][y * rgbpic->linesize[0] + 3 * x + 0] = data[y * 4 * width + 4 * x + 2];
            rgbpic->data[0][y * rgbpic->linesize[0] + 3 * x + 1] = data[y * 4 * width + 4 * x + 1];
            rgbpic->data[0][y * rgbpic->linesize[0] + 3 * x + 2] = data[y * 4 * width + 4 * x + 0];
        }
    }

    sws_scale(swsCtx, rgbpic->data, rgbpic->linesize, 0, height, 
              yuvpic->data, yuvpic->linesize) ;

    AVPacket* pkt = av_packet_alloc() ;
	pkt->data = NULL;
	pkt->size = 0;

	// The PTS of the frame are just in a reference unit,
	// unrelated to the format we are using. We set them,
	// for instance, as the corresponding frame number.
	yuvpic->pts = iframe++ ;

	int ret = avcodec_send_frame(codecContext, yuvpic);
	if (ret < 0) {
		fprintf(stderr, "Error sending frame to codec, errcode = %d\n", ret);
		exit(-1);
	}
	
	ret = avcodec_receive_packet(codecContext, pkt);
	if (ret < 0 && ret != AVERROR(EAGAIN) && ret != AVERROR_EOF)
	{
		fprintf(stderr, "Error receiving packet from codec, errcode = %d\n", ret);
		exit(-1);
	}
	else if (ret >= 0) {
		fflush(stdout);

		// We set the packet PTS and DTS taking in the account our FPS (second argument),
		// and the time base that our selected format uses (third argument).
		av_packet_rescale_ts(pkt, (AVRational){ 1, framerate }, stream->time_base);

		pkt->stream_index = stream->index;

		// Write the encoded frame to the mp4 file.
		av_interleaved_write_frame(formatContext, pkt);

        av_packet_free(&pkt) ;
	}


}
