#include "PillarModel.h"

#include <iostream>

static void fill_and_stroke(cairo_t* context) {
    cairo_set_source_rgb(context,1,1,1) ;
    cairo_fill_preserve(context) ;
    cairo_set_source_rgb(context,0,0,0) ;
    cairo_stroke(context) ;
}

void PillarModel::render(cairo_surface_t* result,
                            int width,
                            int height,
                            bool showForces) {
    float minorWidth = pillarCount + ((float)pillarCount-0.5)*0.85f + 1.0f ;
    float majorWidth = minorWidth + 1.0f ;
    float capHeight = 0.2*majorWidth ;
    float capBase = 5.2f ;
    float scale = (float)width / (1.7*majorWidth) ;
    float lineWidth = 0.14f ;
    cairo_t* context ;

    ensureWorld() ;

    context = cairo_create(result) ;

    cairo_translate(context, width/2, height-50) ;
    cairo_scale(context, scale, -scale) ;
    cairo_set_line_width(context, lineWidth) ;
    cairo_set_miter_limit(context, 2) ;
    cairo_set_line_join(context, CAIRO_LINE_JOIN_MITER) ;

    // Dessine le toit

    cairo_save(context) ;
    
    cairo_translate(context , capBody->GetPosition().x,
                    capBody->GetPosition().y) ;
    cairo_rotate(context, capBody->GetAngle()) ;

    cairo_move_to(context, -majorWidth/2.0f, 0.85) ;
    cairo_line_to(context, 0, 0.85 + capHeight) ;
    cairo_line_to(context, majorWidth/2.0f, 0.85) ;
    cairo_close_path(context) ;

    cairo_rectangle(context, -minorWidth/2, 0, minorWidth, 0.85) ;

    fill_and_stroke(context) ;

    cairo_restore(context) ;

    for (auto i = 0 ; i < pillarCount ; i++) {
        float h = pillarHeights[i] ;
        float w = 0.5f ;

        if (h < 0) {
            h = 0 ;
        } else if (h > 1.0f) {
            h = 1.0f ;
        }
        w -= 0.05f*h ;
        h = 1.0f + 3.2f*h ;

        cairo_save(context) ;

        cairo_translate(context, pillarBodies[i]->GetPosition().x,
                        pillarBodies[i]->GetPosition().y) ;
        cairo_rotate(context, pillarBodies[i]->GetAngle()) ;

        cairo_move_to(context, -0.5f, 0.5f) ;
        cairo_line_to(context, -w, h-0.5f) ;
        cairo_line_to(context,  w, h-0.5f) ;
        cairo_line_to(context,  0.5f, 0.5f) ;
        cairo_close_path(context) ;

        cairo_move_to(context, -0.75f, lineWidth/2) ;
        cairo_line_to(context, -0.75f, 0.3f) ;
        cairo_line_to(context, -0.5f, 0.5f) ;
        cairo_line_to(context,  0.5f, 0.5f) ;
        cairo_line_to(context, 0.75f, 0.3f) ;
        cairo_line_to(context, 0.75f, lineWidth/2) ;
        cairo_close_path(context) ;

        cairo_move_to(context, -0.75f, h-lineWidth/2) ;
        cairo_line_to(context, -0.75f, h-0.3f) ;
        cairo_line_to(context, -0.5f, h-0.5f) ;
        cairo_line_to(context,  0.5f, h-0.5f) ;
        cairo_line_to(context, 0.75f, h-0.3f) ;
        cairo_line_to(context, 0.75f, h-lineWidth/2) ;
        cairo_close_path(context) ;

        fill_and_stroke(context) ;

        cairo_restore(context) ;
    }

    // Dessine la base

    cairo_rectangle(context, -majorWidth/2.0f, 0, majorWidth, 0.5f) ;
    cairo_rectangle(context, -minorWidth/2.0f, 0.5f, minorWidth, 0.5f) ;
    fill_and_stroke(context) ;


    for (auto i = 0 ; i < pillarCount ; i++) {
        cairo_save(context) ;

        cairo_translate(context, labelBodies[i]->GetPosition().x,
                        labelBodies[i]->GetPosition().y) ;
        cairo_rotate(context, labelBodies[i]->GetAngle()) ;

        if (images[i]) {
            cairo_save(context) ;
            cairo_scale(context, 1.0f/128, -1.0f/128) ;
            cairo_set_source_surface(context, images[i],-64, -64) ;
            cairo_paint(context) ;
            cairo_restore(context) ;
        } else {
            cairo_move_to(context, 0, 0) ;
            cairo_arc(context, 0, 0, 0.5f, 0, 2*M_PI) ;
            cairo_set_source_rgb(context, 179.0f/255.0f, 184.0f/255.0f, 193.0f/255.0f) ;
            cairo_fill(context) ;
        }

        cairo_restore(context) ;
    }

    if (showForces) {
        // Affiche les contacts pour le debug

        for (b2ContactEdge* e = capBody->GetContactList() ; e ; e = e->next) {
            b2Contact* c = e->contact ;

            b2WorldManifold worldManifold ;
            b2Manifold* localManifold ;

            c->GetWorldManifold(&worldManifold) ;
            localManifold = c->GetManifold() ;

            if (localManifold->pointCount) {
                b2Vec2 accum = b2Vec2_zero  ;
                
                for (auto i = 0 ; i < localManifold->pointCount; i++) {
                    accum.x += worldManifold.points[i].x / localManifold->pointCount ;
                    accum.y += worldManifold.points[i].y / localManifold->pointCount ;
                }

                cairo_move_to(context, accum.x, accum.y) ;
                cairo_set_line_width(context, 0.04) ;
                cairo_set_source_rgb(context,0,1,0) ;
                cairo_rel_line_to(context, worldManifold.normal.x, worldManifold.normal.y) ;
                cairo_stroke(context) ;
            }

            b2MassData mass ;
            capBody->GetMassData(&mass) ;
            b2Vec2 center = capBody->GetWorldPoint(mass.center) ;
                float px = center.x ;
                float py = center.y ;

                cairo_move_to(context, px, py) ;
                cairo_arc(context, px, py, 0.1, 0, 2*M_PI) ;
                cairo_set_source_rgb(context,1,0,0) ;
                cairo_fill(context) ;
        }
    }

    cairo_destroy(context) ;
}

void PillarModel::buildWorld() {
    float minorWidth = pillarCount + ((float)pillarCount-0.5)*0.85f + 1.0f ;
    float majorWidth = minorWidth + 1.0f ;
    float capHeight = 0.2*majorWidth ;
    float capBase = 5.2f ;

    b2Vec2 points[8] ;
    b2FixtureDef fixtureDef;

    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.8f;
    fixtureDef.restitution = 0.5f ;

    // Crèe un sol
    points[3] = b2Vec2(-majorWidth*2, 0) ;
    points[2] = b2Vec2(majorWidth*2, 0) ;
    points[1] = b2Vec2(majorWidth*2, majorWidth*2) ;
    points[0] = b2Vec2(-majorWidth*2, majorWidth*2) ;
    b2ChainShape groundShape ;
    groundShape.CreateLoop(points, 4) ;
    b2BodyDef groundDef ;
    groundDef.position.Set(0, -0.1f) ;
    b2Body* ground = world->CreateBody(&groundDef) ;
    fixtureDef.shape = &groundShape ;
    fixtureDef.filter.categoryBits = 1<<0 ; // Sol: catégorie 0
    ground->CreateFixture(&fixtureDef) ;

    // Crèe notre chapeau

    fixtureDef.filter.categoryBits = 1<<1 ; // Maçonnerie: catégorie 1
    fixtureDef.filter.maskBits = -1 & (~(1<<2)) ;   // Collisionne avec tout sauf
                                                        // catégorie 2

    points[0] = b2Vec2( minorWidth/2.0f, 0) ;
    points[1] = b2Vec2( minorWidth/2.0f, 0.85) ;
    points[2] = b2Vec2( majorWidth/2.0f, 0.85) ;
    points[3] = b2Vec2(0, 0.85 + capHeight) ;
    points[4] = b2Vec2(-majorWidth/2.0f, 0.85) ;
    points[5] = b2Vec2(-minorWidth/2.0f, 0.85) ;
    points[6] = b2Vec2(-minorWidth/2.0f, 0) ;

    b2BodyDef capDef ;
    capDef.position.Set(0, capBase) ;
    capDef.type = b2_dynamicBody ;
    b2PolygonShape capShape ;
    capShape.Set(points, 7) ;
    capBody = world->CreateBody(&capDef) ;

    fixtureDef.shape = &capShape;
    capBody->CreateFixture(&fixtureDef) ;

    b2PolygonShape labelShape ;

    labelShape.SetAsBox(0.5f, 0.5f) ;

    for (auto i = 0 ; i < pillarCount ; i++) {
        float x = 1.85f*((float)i-(float)(pillarCount-1)/2.0f) ;
        float h = pillarHeights[i] ;
        if (h < 0) {
            h = 0 ;
        } else if (h > 1.0f) {
            h = 1.0f ;
        }
        h = 1.0f + 3.2f*h ;
        points[0] = b2Vec2( 0.75f, 0) ;
        points[1] = b2Vec2( 0.75f, h) ;
        points[2] = b2Vec2(-0.75f, h) ;
        points[3] = b2Vec2(-0.75f, 0) ;

        b2BodyDef pillarDef ;
        pillarDef.position.Set(x, 1) ;
        pillarDef.type = b2_dynamicBody ;
        b2PolygonShape pillarShape ;
        pillarShape.Set(points, 4) ;
        pillarBodies[i] = world->CreateBody(&pillarDef) ;
        fixtureDef.shape = &pillarShape ;
        pillarBodies[i]->CreateFixture(&fixtureDef) ;

        b2BodyDef labelDef ;
        fixtureDef.filter.categoryBits = 1<<1 ; // Labels: catégorie 2
        fixtureDef.filter.maskBits = ((1<<0) | (1<<2)) ;  // Avec sol et labels

        labelDef.position.Set(x, 1+h/2) ;
        labelDef.type = b2_dynamicBody ;
        labelBodies[i] = world->CreateBody(&labelDef) ;
        fixtureDef.shape = &labelShape ;
        labelBodies[i]->CreateFixture(&fixtureDef) ;

        fixtureDef.filter.categoryBits = 1<<1 ; // Maçonnerie: catégorie 1
        fixtureDef.filter.maskBits = -1 & (~(1<<2)) ;   // Collisionne avec tout sauf
                                                        // catégorie 2


    }

    points[0] = b2Vec2( majorWidth/2, 0) ;
    points[1] = b2Vec2( majorWidth/2, 0.5f) ;
    points[2] = b2Vec2( minorWidth/2, 0.5f) ;
    points[3] = b2Vec2( minorWidth/2, 1.0f) ;
    points[4] = b2Vec2(-minorWidth/2, 1.0f) ;
    points[5] = b2Vec2(-minorWidth/2, 0.5f) ;
    points[6] = b2Vec2(-majorWidth/2, 0.5f) ;
    points[7] = b2Vec2(-majorWidth/2, 0) ;
    b2BodyDef baseDef ;
    baseDef.position.Set(0, 0) ;
    baseDef.type = b2_staticBody ;
    b2PolygonShape baseShape ;
    baseShape.Set(points, 8) ;
    baseBody = world->CreateBody(&baseDef) ;
    fixtureDef.shape = &baseShape ;
    baseBody->CreateFixture(&fixtureDef) ;

    b2DistanceJointDef distanceDef ;

    distanceDef.stiffness = 1.0f ;
    distanceDef.damping = 1.0f ;
    distanceDef.minLength = 0.2 ;
    distanceDef.maxLength = 1 ;
    distanceDef.length = 0.3 ;
    // Crèe les contraintes
    for (auto i = 0 ; i < pillarCount ; i++) {
        distanceDef.bodyA = pillarBodies[i] ;
        distanceDef.bodyB = labelBodies[i] ;
        distanceDef.localAnchorA = pillarBodies[i]->GetLocalCenter() ;
        distanceDef.localAnchorA.y += 1 ;
        distanceDef.localAnchorB = b2Vec2_zero ;
        world->CreateJoint(&distanceDef) ;
    }
}
