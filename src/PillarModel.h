#pragma once

#include <cairo.h>

#include "Playground.h"

/**
 * @brief Une classe simulant un monde composé des piliers, du socle et du toit.
 * Sait dessiner l'état du monde sur une surface Cairo.
 */
class PillarModel : public Playground {
    public:
        /**
         * @brief Construit un nouveau modèle.
         * 
         * @param pillarHeights Une liste de hauteurs entre [0,1], de longueur `pillarCount`
         * @param pillarCount Le nombre de piliers
         */
        PillarModel(const float* pillarHeights,
                    int pillarCount = 4) 
            : pillarCount{pillarCount} {
            this->pillarHeights = new float[pillarCount] ;
            pillarBodies = new b2Body*[pillarCount] ;
            labelBodies = new b2Body*[pillarCount] ;

            memcpy(this->pillarHeights, pillarHeights, pillarCount*sizeof(*this->pillarHeights)) ;


            // TODO: unhardcode
            images = new cairo_surface_t*[pillarCount] ;
            bzero(images, pillarCount * sizeof(*images)) ;
            images[0] = cairo_image_surface_create_from_png("modern.png");
            images[1] = cairo_image_surface_create_from_png("fortress.png");
            images[2] = cairo_image_surface_create_from_png("think.png");
            images[3] = cairo_image_surface_create_from_png("shake.png");
        }

        ~PillarModel() {
            if (pillarHeights) {
                delete pillarHeights ;
                delete pillarBodies ;
                delete labelBodies ;
            }
        }

        /**
         * @brief Rend les éléments dans une `cairo_surface_t`.
         * 
         * @param result La surface à utiliser
         * @param width La largeur de cette surface en unités
         * @param height La hauteur de cette surface en unités
         * @param showForces true si on veut les points de contact et les forces (debug)
         */
        void render(cairo_surface_t* result,
                                    int width,
                                    int height,
                                    bool showForces=false) ;

    protected:
        void buildWorld() ; ///< Construction du monde

    private:
        int pillarCount ; ///< Nombre de piliers
        float* pillarHeights ; ///< Hauteur des piliers

        b2Body* capBody = NULL ; ///< Chapeau
        b2Body* baseBody = NULL ; ///< Socle
        b2Body** pillarBodies = NULL ; ///< Piliers
        b2Body** labelBodies = NULL ; ///< Etiquettes

        cairo_surface_t** images = NULL ; ///< Images associées aux étiquettes
} ;
