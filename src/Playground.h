#pragma once

#include <box2d/box2d.h>

/**
 * @brief Implémente un "monde" box2d vide.
 * 
 * Pour l'instant cette classe n'implémente aucune des sémantiques habituelles
 * de C++ (copy constructors, move constructors, assignment, assignment move, ...)
 * car cela nécessite du clonage d'objets box2d qui reste à investiguer.
 * @author JMP
 */
class Playground {
    public:
        // ------------------------------------------------------------
        // Constructeurs

        /**
         * @brief Construit un monde vide
         * 
         * @param velocityIterations Paramètre pour box2d
         * @param positionIterations Paramètre pour box2d
         */
        Playground(int velocityIterations=6, int positionIterations=2) 
            : positionIterations{positionIterations},
            velocityIterations{velocityIterations},
            world{nullptr} {
        }

        // box2d ne semble à priori pas permettre de cloner
        // facilement un b2World, on désactive donc les constructeurs
        // de copie et de move. 
        // TODO investiguer
        Playground(const Playground& other) = delete ;
        Playground(Playground&& other) = delete ;

        virtual ~Playground() {
            if (world) {
                deleteWorld() ;
            }
        }

        /**
         * @brief S'assure que le monde est construit. 
         * 
         * Ce n'est pas extraordinairement élégant et si les classes
         * descendantes ne l'appellent pas ça va casser.
         */
        void ensureWorld() {
            if (!world) {
                t = 0 ;
                resetWorld() ;
            }
        }

        /**
         * @brief Calcule une itération de l'évolution du monde.
         * 
         * @param timestep La durée à simuler.
         */
        void step(float timestep) {
            ensureWorld() ;
            world->Step(timestep, velocityIterations, positionIterations) ;

            t += timestep ;
        }

        /**
         * @brief Reconstruit le monde.
         * 
         */
        void resetWorld() {
            b2Vec2 gravity(0, -9.81f) ;
            deleteWorld() ;

            world = new b2World(gravity) ;
            buildWorld() ;
        }

    protected:

        /**
         * @brief Reconstruit le monde. Implémenté par les classes descendantes.
         * 
         */
        virtual void buildWorld() = 0 ;

        b2World* world ; ///< Le monde simulé
        float t ; ///< Le temps

    private:

        /**
         * @brief Détruit le monde
         */
        void deleteWorld() {
            b2Body* bodies ;
            b2Joint* joints ;

            if (world) {
                bodies = world->GetBodyList() ;
                while (bodies) {
                    b2Body* next = bodies->GetNext() ;
                    world->DestroyBody(bodies) ;
                    bodies = next ;
                }

                delete world ;
            }
        }

        const int velocityIterations ; ///< Paramètre pour box2d
        const int positionIterations ; ///< Paramètre pour box2d
} ;
